package main

import (
	"net/http"
	"time"

	"github.com/gorilla/securecookie"
	"github.com/spf13/viper"
)

const sessionCookieName = "session_data"

type SessionData struct {
	AccountID int64  `json:"account_id"`
	Token     string `json:"token"`
}

func (d SessionData) IsAuthenticated() bool {
	return d.AccountID != 0
}

type SessionHandler struct {
	expiry time.Duration
	sc     *securecookie.SecureCookie
}

func NewSessionHandler() *SessionHandler {
	hashKey := []byte(viper.GetString("session_cookie_hash_key"))
	blockKey := []byte(viper.GetString("session_cookie_block_key"))
	return &SessionHandler{
		expiry: viper.GetDuration("session_expiry"),
		sc:     securecookie.New(hashKey, blockKey),
	}
}

func (s *SessionHandler) Read(r *http.Request) (d SessionData) {
	if cookie, err := r.Cookie(sessionCookieName); err == nil {
		_ = s.sc.Decode(sessionCookieName, cookie.Value, &d)
	}
	return
}

func (s *SessionHandler) Destroy(w http.ResponseWriter) {
	cookie := http.Cookie{
		Name:     sessionCookieName,
		Value:    "",
		Expires:  time.Unix(0, 0),
		HttpOnly: true,
	}
	http.SetCookie(w, &cookie)
}

func (s *SessionHandler) Write(d SessionData, w http.ResponseWriter) {
	value, err := s.sc.Encode(sessionCookieName, d)
	if err != nil {
		panic(err)
	}
	expiration := time.Now().Add(s.expiry)
	cookie := http.Cookie{
		Name:     sessionCookieName,
		Value:    string(value),
		Expires:  expiration,
		HttpOnly: true,
	}
	http.SetCookie(w, &cookie)
}
