package main

//go:generate rice embed-go

import (
	"html/template"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"github.com/GeertJohan/go.rice"
)

// Templates with functions available to them
var (
	templateMap = template.FuncMap{
		"Upper": func(s string) string {
			return strings.ToUpper(s)
		},
	}
	templates   = template.New("").Funcs(templateMap)
	templateBox *rice.Box
)

func newTemplate(path string, _ os.FileInfo, _ error) error {
	if path == "" {
		return nil
	}
	templateString, err := templateBox.String(path)
	if err != nil {
		log.Panicf("Unable to extract: path=%s, err=%s", path, err)
	}
	if _, err = templates.New(filepath.Join("templates", path)).Parse(templateString); err != nil {
		log.Panicf("Unable to parse: path=%s, err=%s", path, err)
	}
	return nil
}

// Render a template given a model
func renderTemplate(w http.ResponseWriter, tmpl string, p interface{}) {
	err := templates.ExecuteTemplate(w, tmpl, p)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

// Load and parse templates (from binary or disk)
func loadTemplates() {
	templateBox = rice.MustFindBox("templates")
	templateBox.Walk("", newTemplate)
}

func init() {
	loadTemplates()
}
