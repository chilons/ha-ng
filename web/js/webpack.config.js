const path = require('path');

module.exports = {
    entry: './main.js',
    output: {
        filename: 'main.dist.js',
        path: path.resolve(__dirname, '../static')
    }
};