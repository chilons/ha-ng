package main

import (
	"github.com/spf13/viper"
	"log"
)

func initConfig() {
	viper.SetConfigName(".ha-ng-web")
	viper.AddConfigPath("$HOME")
	viper.AddConfigPath(".")

	viper.SetDefault("port", "7000")
	viper.SetDefault("api_url", "")
	viper.SetDefault("session_expiry", "720h")
	viper.SetDefault("session_cookie_hash_key", "")
	viper.SetDefault("session_cookie_block_key", "")

	err := viper.ReadInConfig() // Find and read the config file
	if err != nil {             // Handle errors reading the config file
		log.Fatalf("Fatal error config file: %s \n", err)
	}
}

func main() {
	initConfig()
	srv, err := NewServer()
	if err != nil {
		log.Fatal(err)
	}
	srv.Start()
}
