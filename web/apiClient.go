package main

import (
	"HA_NG/models"
	"github.com/dghubble/sling"
	"github.com/spf13/viper"
	"net/http"
)

type ApiErrorResult struct {
	Status string `json:"status"`
	Error  string `json:"error"`
}

func (r *ApiErrorResult) HasError() bool {
	return r.Status != ""
}

type ApiClient struct {
	ApiURL string
	client *sling.Sling
}

func NewApiClient() *ApiClient {
	apiURL := viper.GetString("api_url")
	return &ApiClient{
		ApiURL: apiURL,
		client: sling.New().Base(apiURL),
	}
}

func (c *ApiClient) New(r *http.Request) *UserApiClient {
	return NewUserApiClient(r, c.client)
}

type LoginRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type AuthResult struct {
	AccountID  int64  `json:"account_id"`
	Token      string `json:"token"`
	NewAccount bool   `json:"new_account"`
}

func (c *ApiClient) Login(req LoginRequest) (*AuthResult, error) {
	var auth AuthResult
	_, err := c.client.Post("/auth/local/login").BodyJSON(req).ReceiveSuccess(&auth)
	if err != nil {
		return nil, err
	}
	return &auth, nil
}

func (c *ApiClient) LoginGoogle(idToken string) (*AuthResult, error) {
	var auth AuthResult
	_, err := c.client.Get("/auth/google/callback?id_token=" + idToken).ReceiveSuccess(&auth)
	if err != nil {
		return nil, err
	}
	return &auth, nil
}

func (c *ApiClient) Register(req LoginRequest) (*AuthResult, *ApiErrorResult, error) {
	var auth AuthResult
	var fv ApiErrorResult
	_, err := c.client.Post("/auth/local/register").BodyJSON(req).Receive(&auth, &fv)
	if err != nil {
		return nil, nil, err
	}
	return &auth, &fv, nil
}

func (c *ApiClient) SendPasswordReset(email string) (ok bool, err error) {
	var req struct {
		Email string `json:"email"`
	}
	req.Email = email
	resp, err := c.client.Post("/auth/local/forgot-password").BodyJSON(req).ReceiveSuccess(nil)
	ok = resp.StatusCode == http.StatusOK
	return
}

type ResetPasswordRequest struct {
	Email     string `json:"email"`
	Password  string `json:"password"`
	ResetCode string `json:"reset_code"`
}

func (c *ApiClient) ResetPassword(req *ResetPasswordRequest) (ok bool, err error) {
	resp, err := c.client.Post("/auth/local/reset-password").BodyJSON(req).ReceiveSuccess(nil)
	ok = resp.StatusCode == http.StatusOK
	return
}

type UserApiClient struct {
	AccountID int64
	client    *sling.Sling
}

func NewUserApiClient(r *http.Request, c *sling.Sling) *UserApiClient {
	sd := sessionData(r)
	return &UserApiClient{
		AccountID: sd.AccountID,
		client:    c.New().Set("Authorization", "Bearer "+sd.Token),
	}
}

func (c *UserApiClient) GetProfile() (*models.Profile, error) {
	var profile models.Profile
	_, err := c.client.Get("/api/profile").ReceiveSuccess(&profile)
	return &profile, err
}

func (c *UserApiClient) UpdateProfile(p *models.Profile) (*models.Profile, *ApiErrorResult, error) {
	var sv models.Profile
	var fv ApiErrorResult
	_, err := c.client.Put("/api/profile").BodyJSON(p).Receive(&sv, &fv)
	if err != nil {
		return nil, nil, err
	}
	return &sv, &fv, nil
}
