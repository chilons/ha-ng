// Package app ties together application resources and handlers.
package main

import (
	"HA_NG/models"
	"context"
	"errors"
	"github.com/sirupsen/logrus"
	"net/http"
	"strings"

	"github.com/go-chi/chi"

	"HA_NG/logging"
)

const sessionCtxKey = "session"

var (
	errInternal = errors.New("Internal Server Error")
)

// LocalAuth provides application resources and handlers.
type WebApp struct {
	client  *ApiClient
	session *SessionHandler
}

// NewWebApp configures and returns the web instance.
func NewWebApp(client *ApiClient) *WebApp {
	return &WebApp{client, NewSessionHandler()}
}

// Router provides application routes.
func (rs *WebApp) Router() *chi.Mux {
	r := chi.NewRouter()
	r.Use(rs.sessionCtx)
	r.Get("/", rs.renderIndex)
	r.Post("/", rs.renderLogin)
	r.Get("/register", rs.renderRegister)
	r.Post("/register", rs.renderRegister)
	r.Get("/edit-profile", rs.renderEditProfile)
	r.Post("/edit-profile", rs.renderEditProfile)
	r.Get("/forgot-password", rs.renderForgotPassword)
	r.Post("/forgot-password", rs.renderForgotPassword)
	r.Get("/reset-password", rs.renderResetPassword)
	r.Post("/reset-password", rs.renderResetPassword)
	r.Post("/logout", rs.logout)
	r.Get("/login-google", rs.loginGoogle)
	return r
}

func (rs *WebApp) log(r *http.Request) logrus.FieldLogger {
	return logging.GetLogEntry(r)
}

func (rs *WebApp) sessionCtx(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		sd := rs.session.Read(r)
		ctx := context.WithValue(r.Context(), sessionCtxKey, sd)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

// Render a template given a model
func (rs *WebApp) renderTemplate(w http.ResponseWriter, tmpl string, p interface{}) {
	err := templates.ExecuteTemplate(w, tmpl, p)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func (rs *WebApp) renderError(w http.ResponseWriter, err error) {
	var m struct {
		Error error
	}
	m.Error = err
	renderTemplate(w, "templates/error.html", &m)
}

func (rs *WebApp) renderIndex(w http.ResponseWriter, r *http.Request) {
	if sessionData(r).IsAuthenticated() {
		rs.renderProfile(w, r)
	} else {
		rs.renderLogin(w, r)
	}
}

func (rs *WebApp) authenticate(auth *AuthResult, w http.ResponseWriter) {
	var s SessionData
	s.AccountID = auth.AccountID
	s.Token = auth.Token
	rs.session.Write(s, w)
}

func (rs *WebApp) renderLogin(w http.ResponseWriter, r *http.Request) {
	var m struct {
		Title        string
		IsLoggingIn  bool
		InvalidLogin bool
		Form         LoginRequest
	}
	m.Title = "Demo"
	m.IsLoggingIn = r.Method == "POST"

	if m.IsLoggingIn {
		_ = r.ParseForm()
		m.Form.Email = r.FormValue("email")
		m.Form.Password = r.FormValue("password")

		auth, err := rs.client.Login(m.Form)
		if err != nil {
			rs.log(r).Error(err)
			rs.renderError(w, errInternal)
			return
		}
		if auth.AccountID != 0 {
			rs.authenticate(auth, w)
			http.Redirect(w, r, "/", 301)
			return
		} else {
			m.InvalidLogin = true
		}
	}

	renderTemplate(w, "templates/login.html", &m)
}

func (rs *WebApp) renderRegister(w http.ResponseWriter, r *http.Request) {
	var m struct {
		Title        string
		IsSubmitting bool
		HasError     bool
		ErrorMessage string
		Form         LoginRequest
	}
	m.Title = "Register"
	m.IsSubmitting = r.Method == "POST"

	if m.IsSubmitting {
		_ = r.ParseForm()
		m.Form.Email = r.FormValue("email")
		m.Form.Password = r.FormValue("password")
		m.HasError = r.FormValue("confirm_password") != m.Form.Password
		if m.HasError {
			m.ErrorMessage = "Passwords not matching."
		} else {
			auth, fv, err := rs.client.Register(m.Form)
			if err != nil {
				rs.log(r).Error(err)
				rs.renderError(w, errInternal)
				return
			}
			if fv.HasError() {
				m.HasError = true
				m.ErrorMessage = fv.Error
			} else {
				rs.authenticate(auth, w)
				http.Redirect(w, r, "/edit-profile?register=1", 301)
				return
			}
		}
	}

	renderTemplate(w, "templates/register.html", &m)
}

func (rs *WebApp) renderProfile(w http.ResponseWriter, r *http.Request) {
	var m struct {
		Title   string
		Profile *models.Profile
	}
	m.Title = "Your Profile"
	profile, err := rs.client.New(r).GetProfile()
	if err != nil {
		rs.log(r).Error(err)
		rs.renderError(w, errInternal)
		return
	}
	m.Profile = profile
	renderTemplate(w, "templates/profile.html", &m)
}

func (rs *WebApp) logout(w http.ResponseWriter, r *http.Request) {
	rs.session.Destroy(w)
	http.Redirect(w, r, "/", 301)
}

func (rs *WebApp) loginGoogle(w http.ResponseWriter, r *http.Request) {
	idToken := r.FormValue("id_token")
	auth, err := rs.client.LoginGoogle(idToken)
	if err != nil {
		rs.log(r).Error(err)
		rs.renderError(w, errInternal)
		return
	}
	rs.authenticate(auth, w)
	if auth.NewAccount {
		http.Redirect(w, r, "/edit-profile?register=1", 301)
	} else {
		http.Redirect(w, r, "/", 301)
	}
	return
}

func (rs *WebApp) renderEditProfile(w http.ResponseWriter, r *http.Request) {
	c := rs.client.New(r)
	var m struct {
		Title        string
		IsSubmitting bool
		IsRegister   bool
		Form         *models.Profile
		HasError     bool
		ErrorMessage string
	}
	m.IsSubmitting = r.Method == "POST"
	m.IsRegister = r.URL.Query().Get("register") != ""
	m.Title = "Edit Profile"
	if m.IsRegister {
		m.Title = "Fill Profile Information"
	}
	profile, err := c.GetProfile()
	if err != nil {
		rs.log(r).Error(err)
		rs.renderError(w, errInternal)
		return
	}
	m.Form = profile

	if m.IsSubmitting {
		_ = r.ParseForm()
		m.Form.Email = r.FormValue("email")
		m.Form.FullName = r.FormValue("full_name")
		m.Form.Address = r.FormValue("address")
		m.Form.Telephone = r.FormValue("telephone")
		m.Form.Telephone = strings.Replace(m.Form.Telephone, " ", "", -1)

		_, fv, err := c.UpdateProfile(m.Form)
		if err != nil {
			rs.log(r).Error(err)
			rs.renderError(w, errInternal)
			return
		}
		if fv.HasError() {
			m.HasError = true
			m.ErrorMessage = fv.Error
		} else {
			if m.IsRegister {
				http.Redirect(w, r, "/", 301)
			}
		}
	}
	renderTemplate(w, "templates/profile-edit.html", &m)
}

func (rs *WebApp) renderForgotPassword(w http.ResponseWriter, r *http.Request) {
	var m struct {
		Title        string
		IsSubmitting bool
		IsDone       bool
		Email        string
	}
	m.Title = "Forgot Password"
	m.IsSubmitting = r.Method == "POST"

	if m.IsSubmitting {
		_ = r.ParseForm()
		m.Email = r.FormValue("email")

		ok, err := rs.client.SendPasswordReset(m.Email)
		if err != nil {
			rs.log(r).Error(err)
			rs.renderError(w, errInternal)
			return
		}
		m.IsDone = ok
	}

	renderTemplate(w, "templates/forgot.html", &m)
}

func (rs *WebApp) renderResetPassword(w http.ResponseWriter, r *http.Request) {
	var m struct {
		Title        string
		IsSubmitting bool
		HasError     bool
		ErrorMessage string
		Form         ResetPasswordRequest
	}
	m.Title = "Reset Password"
	m.IsSubmitting = r.Method == "POST"

	if m.IsSubmitting {
		_ = r.ParseForm()
		m.Form.Email = r.FormValue("email")
		m.Form.ResetCode = r.FormValue("code")
		m.Form.Password = r.FormValue("password")
		m.HasError = r.FormValue("confirm_password") != m.Form.Password
		if m.HasError {
			m.ErrorMessage = "Passwords not matching."
		} else {
			ok, err := rs.client.ResetPassword(&m.Form)
			if err != nil {
				rs.log(r).Error(err)
				rs.renderError(w, errInternal)
				return
			}
			if !ok {
				m.HasError = true
				m.ErrorMessage = "Invalid Password"
			} else {
				http.Redirect(w, r, "/", 301)
				return
			}
		}
	}

	renderTemplate(w, "templates/forgot-reset.html", &m)
}

func sessionData(r *http.Request) SessionData {
	return r.Context().Value(sessionCtxKey).(SessionData)
}
