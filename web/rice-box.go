package main

import (
	"time"

	"github.com/GeertJohan/go.rice/embedded"
)

func init() {

	// define files
	file2 := &embedded.EmbeddedFile{
		Filename:    "error.html",
		FileModTime: time.Unix(1546867314, 0),

		Content: string("<!DOCTYPE html>\n<html lang=\"en\">\n<head>\n    <meta charset=\"UTF-8\">\n    <title>Error</title>\n</head>\n<body>\n    {{ .Error }}\n</body>\n</html>"),
	}
	file3 := &embedded.EmbeddedFile{
		Filename:    "footer.html",
		FileModTime: time.Unix(1546885151, 0),

		Content: string("    <script>\n        function onSuccess(googleUser) {\n            console.log('Logged in as: ' + googleUser.getBasicProfile().getName());\n            var idToken = googleUser.getAuthResponse().id_token;\n            window.onbeforeunload = function(e){\n                gapi.auth2.getAuthInstance().signOut();\n            };\n            window.location.replace(\"/login-google?id_token=\" + idToken)\n        }\n        function onFailure(error) {\n            console.log(error);\n        }\n        function renderButton() {\n            gapi.signin2.render('google-login', {\n                'scope': 'profile email',\n                'width': 240,\n                'height': 50,\n                'longtitle': true,\n                'theme': 'dark',\n                'onsuccess': onSuccess,\n                'onfailure': onFailure\n            });\n        }\n    </script>\n\n    <script src=\"/static/main.dist.js\"></script>\n    <script src=\"https://apis.google.com/js/platform.js?onload=renderButton\" async defer></script>\n    <script src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyBOWyiG2zTfhd978O2DxQbxwQMKgPEdY04&libraries=places\"></script>\n    </body>\n</html>\n"),
	}
	file4 := &embedded.EmbeddedFile{
		Filename:    "forgot-reset.html",
		FileModTime: time.Unix(1546878858, 0),

		Content: string("{{ template \"templates/header.html\" . }}\n\n<div class=\"wrapper\" >\n    <div class=\"container center\">\n        <h2>Reset Password</h2>\n        <form autocomplete=\"off\" action=\"\" method=\"post\">\n            <div class=\"group\">\n                <label for=\"password\">Password:</label>\n                <input id=\"password\" type=\"password\" name=\"password\">\n            </div>\n            <div class=\"group\">\n                <label for=\"confirm_password\">Confirm Password:</label>\n                <input id=\"confirm_password\" type=\"password\" name=\"confirm_password\">\n            </div>\n            <input type=\"submit\" value=\"Reset Password\" id=\"submit\">\n            {{ if .HasError }}\n                <div class=\"error-text\">{{.ErrorMessage}}</div>\n            {{ end }}\n        </form>\n    </div>\n</div>\n\n{{ template \"templates/footer.html\" . }}\n"),
	}
	file5 := &embedded.EmbeddedFile{
		Filename:    "forgot.html",
		FileModTime: time.Unix(1546878999, 0),

		Content: string("{{ template \"templates/header.html\" . }}\n\n<div class=\"wrapper\" >\n    <div class=\"container center\">\n        {{ if .IsDone }}\n            An email containing password reset instructions has been sent to <strong>{{.Email}}</strong>. Please\n            check your mail inbox in a moment.\n        {{ else }}\n            <h2>Forgot Password</h2>\n            <form autocomplete=\"off\" action=\"\" method=\"post\">\n                <div class=\"group\">\n                    <label for=\"email\">Email:</label>\n                    <input type=\"email\" id=\"email\" name=\"email\" value=\"{{.Email}}\">\n                </div>\n                <input type=\"submit\" value=\"Send Email\" id=\"submit\">\n                {{ if .IsSubmitting }}\n                    <div class=\"error-text\">Account does not exist.</div>\n                {{ end }}\n            </form>\n        {{ end }}\n    </div>\n</div>\n\n{{ template \"templates/footer.html\" . }}\n"),
	}
	file6 := &embedded.EmbeddedFile{
		Filename:    "header.html",
		FileModTime: time.Unix(1546854497, 0),

		Content: string("<html>\n  <head>\n    <link rel=\"stylesheet\" href=\"/static/style.css\">\n    <meta name=\"google-signin-client_id\" content=\"1056080587894-kp7dmurov45kgd9726nvm6abqt3iv0ao.apps.googleusercontent.com\">\n    <title>{{ .Title }}</title>\n  </head>\n  <body>\n"),
	}
	file7 := &embedded.EmbeddedFile{
		Filename:    "invalid.html",
		FileModTime: time.Unix(1546849439, 0),

		Content: string("This is an intentionally invalid template:\n\n{{ .intentionallyUndefinedField }}\n\n"),
	}
	file8 := &embedded.EmbeddedFile{
		Filename:    "login.html",
		FileModTime: time.Unix(1546874252, 0),

		Content: string("{{ template \"templates/header.html\" . }}\n\n<div class=\"wrapper\" >\n    <div class=\"container center\">\n        <div id=\"google-login\" class=\"google-login social-login\">\n            Sign in With Google\n        </div>\n        <p class=\"separator\" >-OR-</p>\n        <form autocomplete=\"off\" action=\"\" method=\"post\">\n            <div class=\"group\">\n                <label for=\"email\">Email:</label>\n                <input type=\"email\" id=\"email\" name=\"email\" value=\"{{.Form.Email}}\">\n            </div>\n            <div class=\"group\">\n                <label for=\"password\">Password:</label>\n                <input id=\"password\" type=\"password\" name=\"password\">\n            </div>\n            <a href=\"/forgot-password\" class=\"form-link\">Forgot password?</a>\n            <a href=\"/register\" class=\"form-link\">Create an Account</a>\n            <input type=\"submit\" value=\"Login\" id=\"submit\">\n            {{ if .InvalidLogin }}\n                <div class=\"error-text\">Invalid Email or Password</div>\n            {{ end }}\n        </form>\n    </div>\n</div>\n\n{{ template \"templates/footer.html\" . }}\n"),
	}
	file9 := &embedded.EmbeddedFile{
		Filename:    "profile-edit.html",
		FileModTime: time.Unix(1546887405, 0),

		Content: string("{{ template \"templates/header.html\" . }}\n\n<div class=\"wrapper\" >\n    <div class=\"container\">\n        {{ if not .IsRegister }}\n            <div class=\"nav\">\n                <a href=\"/\">Home</a>\n            </div>\n        {{ end }}\n        <h2>{{.Title}}</h2>\n        <form autocomplete=\"off\" action=\"\" method=\"post\">\n            {{ if not .IsRegister }}\n                <div class=\"group\">\n                    <label for=\"email\">Email:</label>\n                    <input type=\"email\" id=\"email\" name=\"email\" value=\"{{.Form.Email}}\">\n                </div>\n            {{ else }}\n                <input type=\"hidden\" id=\"email\" name=\"email\" value=\"{{.Form.Email}}\">\n            {{ end }}\n            <div class=\"group\">\n                <label for=\"full_name\">Full Name:</label>\n                <input type=\"text\" id=\"full_name\" name=\"full_name\" value=\"{{.Form.FullName}}\">\n            </div>\n            <div class=\"group\">\n                <label for=\"email\">Address:</label>\n                <input type=\"text\" id=\"address\" name=\"address\" class=\"address-autocomplete\" value=\"{{.Form.Address}}\">\n            </div>\n            <div class=\"group\">\n                <label for=\"telephone\">Telephone:</label>\n                <input type=\"text\" id=\"telephone\" name=\"telephone\" value=\"{{.Form.Telephone}}\">\n            </div>\n            <input type=\"submit\" value=\"Save\" id=\"submit\">\n            {{ if .HasError }}\n                <div class=\"error-text\">{{.ErrorMessage}}</div>\n            {{ else if .IsSubmitting }}\n                <div class=\"success-text\">Profile Saved Successfully</div>\n            {{ end }}\n        </form>\n    </div>\n</div>\n\n{{ template \"templates/footer.html\" . }}\n"),
	}
	filea := &embedded.EmbeddedFile{
		Filename:    "profile.html",
		FileModTime: time.Unix(1546873933, 0),

		Content: string("{{ template \"templates/header.html\" . }}\n\n<div class=\"wrapper\" >\n    <div class=\"container\">\n        <div class=\"nav\">\n            Welcome, {{ .Profile.FullName }}\n            <form method=\"post\" action=\"/logout\" class=\"logout\"><button type=\"submit\">Logout</button></form>\n        </div>\n        <h2>Your Profile</h2>\n        <dl class=\"table profile-info\">\n            <dt>Full Name</dt>\n            <dd>{{ .Profile.FullName }}</dd>\n            <dt>Email</dt>\n            <dd>{{ .Profile.Email }}</dd>\n            <dt>Address</dt>\n            <dd>{{ .Profile.Address }}</dd>\n            <dt>Telephone</dt>\n            <dd>{{ .Profile.Telephone }}</dd>\n        </dl>\n        <a href=\"/edit-profile\">Edit</a>\n    </div>\n</div>\n\n{{ template \"templates/footer.html\" . }}\n"),
	}
	fileb := &embedded.EmbeddedFile{
		Filename:    "register.html",
		FileModTime: time.Unix(1546877247, 0),

		Content: string("{{ template \"templates/header.html\" . }}\n\n<div class=\"wrapper\" >\n    <div class=\"container center\">\n        <h2>Register</h2>\n        <form autocomplete=\"off\" action=\"\" method=\"post\">\n            <div class=\"group\">\n                <label for=\"email\">Email:</label>\n                <input type=\"email\" id=\"email\" name=\"email\" value=\"{{.Form.Email}}\">\n            </div>\n            <div class=\"group\">\n                <label for=\"password\">Password:</label>\n                <input id=\"password\" type=\"password\" name=\"password\">\n            </div>\n            <div class=\"group\">\n                <label for=\"confirm_password\">Confirm Password:</label>\n                <input id=\"confirm_password\" type=\"password\" name=\"confirm_password\">\n            </div>\n            <input type=\"submit\" value=\"Register\" id=\"submit\">\n            {{ if .HasError }}\n                <div class=\"error-text\">{{.ErrorMessage}}</div>\n            {{ end }}\n        </form>\n    </div>\n</div>\n\n{{ template \"templates/footer.html\" . }}\n"),
	}

	// define dirs
	dir1 := &embedded.EmbeddedDir{
		Filename:   "",
		DirModTime: time.Unix(1546887405, 0),
		ChildFiles: []*embedded.EmbeddedFile{
			file2, // "error.html"
			file3, // "footer.html"
			file4, // "forgot-reset.html"
			file5, // "forgot.html"
			file6, // "header.html"
			file7, // "invalid.html"
			file8, // "login.html"
			file9, // "profile-edit.html"
			filea, // "profile.html"
			fileb, // "register.html"

		},
	}

	// link ChildDirs
	dir1.ChildDirs = []*embedded.EmbeddedDir{}

	// register embeddedBox
	embedded.RegisterEmbeddedBox(`templates`, &embedded.EmbeddedBox{
		Name: `templates`,
		Time: time.Unix(1546887405, 0),
		Dirs: map[string]*embedded.EmbeddedDir{
			"": dir1,
		},
		Files: map[string]*embedded.EmbeddedFile{
			"error.html":        file2,
			"footer.html":       file3,
			"forgot-reset.html": file4,
			"forgot.html":       file5,
			"header.html":       file6,
			"invalid.html":      file7,
			"login.html":        file8,
			"profile-edit.html": file9,
			"profile.html":      filea,
			"register.html":     fileb,
		},
	})
}
