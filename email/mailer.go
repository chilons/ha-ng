package email

import (
	"fmt"
	"github.com/sendgrid/sendgrid-go"
	"github.com/sendgrid/sendgrid-go/helpers/mail"
	"github.com/spf13/viper"
)

type Mailer struct {
	client         *sendgrid.Client
	from           *mail.Email
	destinationURI string
}

func NewMailer(name string, address string) *Mailer {
	apiKey := viper.GetString("email_sendgrid_api_key")
	destinationURI := viper.GetString("email_destination_uri")
	return &Mailer{
		client:         sendgrid.NewSendClient(apiKey),
		from:           mail.NewEmail(name, address),
		destinationURI: destinationURI,
	}
}

func NewNoreplyMailer() *Mailer {
	name := viper.GetString("email_noreply_name")
	address := viper.GetString("email_noreply_address")
	return NewMailer(name, address)
}

func (c *Mailer) SendResetPasswordMail(to *mail.Email, code string) error {
	subject := "Reset Password"
	link := fmt.Sprintf("%s/reset-password?email=%s&code=%s", c.destinationURI, to.Address, code)
	plainTextContent := fmt.Sprintf("Here is your reset password link: %s", link)
	message := mail.NewSingleEmail(c.from, subject, to, plainTextContent, plainTextContent)
	_, err := c.client.Send(message)
	return err
}

func NewEmail(name string, address string) *mail.Email {
	return mail.NewEmail(name, address)
}
