package cmd

import (
	"log"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	"HA_NG/api"
)

// serveCmd represents the serve command
var serveCmd = &cobra.Command{
	Use:   "serve",
	Short: "start http server with configured api",
	Long:  `Starts a http server and serves the configured api`,
	Run: func(cmd *cobra.Command, args []string) {
		server, err := api.NewServer()
		if err != nil {
			log.Fatal(err)
		}
		server.Start()
	},
}

func init() {
	RootCmd.AddCommand(serveCmd)

	// flags and configuration settings.
	viper.SetDefault("port", "8000")
	viper.SetDefault("log_level", "debug")

	viper.SetDefault("auth_jwt_secret", "secret123")
	viper.SetDefault("auth_jwt_expiry", "720h")

	viper.SetDefault("email_sendgrid_api_key", "")
	viper.SetDefault("email_destination_uri", "")
	viper.SetDefault("email_noreply_name", "Noreply")
	viper.SetDefault("email_noreply_address", "")
}
