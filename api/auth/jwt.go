package auth

import (
	"HA_NG/models"
	"context"
	"github.com/spf13/viper"
	"net/http"
	"time"

	"github.com/go-chi/jwtauth"
	"github.com/go-chi/render"

	"HA_NG/logging"
)

type ContextKey string

const (
	AuthCtxKey ContextKey = "auth"
)

// JwtAuthenticator is a default authentication middleware to enforce access from the
// Verifier middleware request context values. The JwtAuthenticator sends a 401 Unauthorized
// response for any unverified tokens and passes the good ones through.
func JwtAuthenticator(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		token, claims, err := jwtauth.FromContext(r.Context())

		if err != nil {
			logging.GetLogEntry(r).Warn(err)
			render.Render(w, r, ErrUnauthorized(ErrTokenUnauthorized))
			return
		}

		if !token.Valid {
			render.Render(w, r, ErrUnauthorized(ErrTokenExpired))
			return
		}

		// Token is valid, parse Claims
		var sd SessionData
		err = sd.ParseClaims(claims)
		if err != nil {
			logging.GetLogEntry(r).Error(err)
			render.Render(w, r, ErrUnauthorized(ErrInvalidAccessToken))
			return
		}

		// Save authentication data into context
		ctx := context.WithValue(r.Context(), AuthCtxKey, sd)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

// JwtAuth implements JWT authentication flow.
type JwtAuth struct {
	JwtAuth          *jwtauth.JWTAuth
	JwtExpiry        time.Duration
	JwtRefreshExpiry time.Duration
}

// NewJwtAuth configures and returns a JWT authentication instance.
func NewJwtAuth() (*JwtAuth, error) {
	secret := viper.GetString("auth_jwt_secret")
	a := &JwtAuth{
		JwtAuth:          jwtauth.New("HS256", []byte(secret), nil),
		JwtExpiry:        viper.GetDuration("auth_jwt_expiry"),
		JwtRefreshExpiry: viper.GetDuration("auth_jwt_refresh_expiry"),
	}
	return a, nil
}

// Verifier http middleware will verify a jwt string from a http request.
func (a *JwtAuth) Verifier() func(http.Handler) http.Handler {
	return jwtauth.Verifier(a.JwtAuth)
}

// CreateToken returns an access token for provided account Claims.
func (a *JwtAuth) CreateToken(c jwtauth.Claims) (string, error) {
	c.SetIssuedNow()
	c.SetExpiryIn(a.JwtExpiry)
	_, tokenString, err := a.JwtAuth.Encode(c)
	return tokenString, err
}

func (a *JwtAuth) CreateTokenForAccount(acc *models.Account) (string, error) {
	sd := SessionData{AccountID: acc.ID}
	return a.CreateToken(sd.Claims())
}
