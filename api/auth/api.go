package auth

import (
	"HA_NG/database"
	"github.com/go-chi/chi"
	"github.com/jmoiron/sqlx"
	"github.com/sirupsen/logrus"
	"net/http"

	"HA_NG/logging"
)

// API provides auth resources and handlers.
type API struct {
	TokenAuth *JwtAuth
	local     *LocalAuth
	google    *GoogleOAuth
}

// NewAPI configures and returns the auth API.
func NewAPI(db *sqlx.DB) (*API, error) {
	authStore := database.NewAuthStore(db)
	tokenAuth, err := NewJwtAuth()
	if err != nil {
		return nil, err
	}
	api := &API{
		TokenAuth: tokenAuth,
		local:     NewLocalAuth(authStore, tokenAuth),
		google:    NewGoogleOAuth(authStore, tokenAuth),
	}
	return api, nil
}

// Router provides auth routes.
func (a *API) Router() *chi.Mux {
	r := chi.NewRouter()
	r.Mount("/local", a.local.Router())
	r.Mount("/google", a.google.Router())

	return r
}

func log(r *http.Request) logrus.FieldLogger {
	return logging.GetLogEntry(r)
}
