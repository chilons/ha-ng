package auth

import (
	"HA_NG/models"
	"errors"
	"github.com/dgrijalva/jwt-go"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"google.golang.org/api/oauth2/v2"

	"HA_NG/database"
)

var httpClient = &http.Client{}

// GoogleOAuth provides google oauth resources and handlers
type GoogleOAuth struct {
	store     *database.AuthStore
	tokenAuth *JwtAuth
}

// NewGoogleOAuth configures and returns GoogleOAuth
func NewGoogleOAuth(store *database.AuthStore, tokenAuth *JwtAuth) *GoogleOAuth {
	return &GoogleOAuth{store, tokenAuth}
}

// Router provides application routes.
func (rs *GoogleOAuth) Router() *chi.Mux {
	r := chi.NewRouter()
	r.Get("/callback", rs.callback)
	return r
}

func (rs *GoogleOAuth) verifyIdToken(idToken string) (*oauth2.Tokeninfo, error) {
	oauth2Service, err := oauth2.New(httpClient)
	tokenInfoCall := oauth2Service.Tokeninfo()
	tokenInfoCall.IdToken(idToken)
	tokenInfo, err := tokenInfoCall.Do()
	if err != nil {
		return nil, err
	}
	return tokenInfo, nil
}

type googleTokenInfo struct {
	Iss string `json:"iss"`
	// userId
	Sub string `json:"sub"`
	Azp string `json:"azp"`
	// clientId
	Aud string `json:"aud"`
	Iat int64  `json:"iat"`
	// expired time
	Exp int64 `json:"exp"`

	Email         string `json:"email"`
	EmailVerified bool   `json:"email_verified"`
	AtHash        string `json:"at_hash"`
	Name          string `json:"name"`
	GivenName     string `json:"given_name"`
	FamilyName    string `json:"family_name"`
	Picture       string `json:"picture"`
	Local         string `json:"locale"`
	jwt.StandardClaims
}

// verifyIdToken doesn't return all the info so we have to make this
func (rs *GoogleOAuth) decodeIdToken(idToken string) (*googleTokenInfo, error) {
	token, _, err := new(jwt.Parser).ParseUnverified(idToken, &googleTokenInfo{})
	if err != nil {
		return nil, err
	}
	if tokenInfo, ok := token.Claims.(*googleTokenInfo); ok {
		return tokenInfo, nil
	} else {
		return nil, errors.New("failed to decode id_token")
	}
}

func (rs *GoogleOAuth) callback(w http.ResponseWriter, r *http.Request) {
	idToken := r.URL.Query().Get("id_token")
	isRegister := false
	_, err := rs.verifyIdToken(idToken)
	if err != nil {
		render.Render(w, r, ErrInvalid(err))
		return
	}
	info, err := rs.decodeIdToken(idToken)
	if err != nil {
		render.Render(w, r, ErrInvalid(err))
		return
	}
	acc, err := rs.store.GetAccountByEmail(info.Email)
	if err != nil {
		log(r).Error(err)
		render.Render(w, r, ErrInternalServerError)
		return
	}
	if acc == nil {
		isRegister = true
		acc = &models.Account{Email: info.Email}
		p := &models.Profile{FullName: info.GivenName + " " + info.FamilyName}
		err = rs.store.CreateAccountWithProfile(acc, p)
		if err != nil {
			log(r).Error(err)
			render.Render(w, r, ErrInternalServerError)
			return
		}
	}
	token, err := rs.tokenAuth.CreateTokenForAccount(acc)
	if err != nil {
		log(r).Error(err)
		render.Render(w, r, ErrInternalServerError)
		return
	}
	render.Respond(w, r, &authResponse{
		AccountID:  acc.ID,
		Token:      token,
		NewAccount: isRegister,
	})
	return
}
