package auth

import (
	"context"
	"errors"
	"github.com/go-chi/jwtauth"
)

type SessionData struct {
	AccountID int64
}

func (sd *SessionData) ParseClaims(claims jwtauth.Claims) error {
	id, ok := claims.Get("account_id")
	if !ok {
		return errors.New("could not parse account_id")
	}
	sd.AccountID = int64(id.(float64))
	return nil
}

func (sd *SessionData) Claims() jwtauth.Claims {
	return map[string]interface{}{
		"account_id": sd.AccountID,
	}
}

func SessionDataFromContext(ctx context.Context) SessionData {
	return ctx.Value(AuthCtxKey).(SessionData)
}
