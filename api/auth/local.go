package auth

import (
	"crypto/rand"
	"net/http"
	"strings"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"golang.org/x/crypto/bcrypt"

	"HA_NG/database"
	"HA_NG/email"
	"HA_NG/models"
)

// LocalAuth provides application resources and handlers.
type LocalAuth struct {
	store     *database.AuthStore
	tokenAuth *JwtAuth
	mailer    *email.Mailer
}

// NewAPI configures and returns application LocalAuth.
func NewLocalAuth(store *database.AuthStore, tokenAuth *JwtAuth) *LocalAuth {
	return &LocalAuth{
		store:     store,
		tokenAuth: tokenAuth,
		mailer:    email.NewNoreplyMailer(),
	}
}

// Router provides application routes.
func (rs *LocalAuth) Router() *chi.Mux {
	r := chi.NewRouter()
	r.Post("/register", rs.register)
	r.Post("/login", rs.login)
	r.Post("/forgot-password", rs.forgotPassword)
	r.Post("/reset-password", rs.resetPassword)
	return r
}

func (rs *LocalAuth) hashPassword(password string) string {
	h, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		panic(err)
	}
	return string(h)
}

func (rs *LocalAuth) checkPassword(password string, hashedPassword string) bool {
	return bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password)) == nil
}

var passwordLengthValidation = validation.Length(6, 256)

type registerRequest struct {
	Email    string
	Password string
}

func (body *registerRequest) Bind(r *http.Request) error {
	body.Email = strings.ToLower(strings.TrimSpace(body.Email))
	return validation.ValidateStruct(body,
		validation.Field(&body.Email, validation.Required, is.Email),
		validation.Field(&body.Password, validation.Required, passwordLengthValidation),
	)
}

type authResponse struct {
	AccountID  int64  `json:"account_id"`
	Token      string `json:"token"`
	NewAccount bool   `json:"new_account"`
}

func (rs *LocalAuth) register(w http.ResponseWriter, r *http.Request) {
	body := &registerRequest{}
	if err := render.Bind(r, body); err != nil {
		render.Render(w, r, ErrInvalid(err))
		return
	}

	acc, err := rs.store.GetAccountByEmail(body.Email)
	if err != nil {
		log(r).Error(err)
		render.Render(w, r, ErrInternalServerError)
		return
	}
	if acc != nil {
		render.Render(w, r, ErrInvalid(ErrEmailInUse))
		return
	}

	acc = &models.Account{
		Email:    body.Email,
		Password: rs.hashPassword(body.Password),
	}
	err = rs.store.CreateAccount(acc)
	if err != nil {
		log(r).Error(err)
		render.Render(w, r, ErrInternalServerError)
		return
	}

	token, err := rs.tokenAuth.CreateTokenForAccount(acc)
	if err != nil {
		log(r).Error(err)
		render.Render(w, r, ErrInternalServerError)
		return
	}
	render.Respond(w, r, &authResponse{
		AccountID:  acc.ID,
		Token:      token,
		NewAccount: true,
	})
	return
}

type loginRequest struct {
	Email    string
	Password string
}

func (body *loginRequest) Bind(r *http.Request) error {
	return nil
}

func (rs *LocalAuth) login(w http.ResponseWriter, r *http.Request) {
	body := &loginRequest{}
	if err := render.Bind(r, body); err != nil {
		render.Render(w, r, ErrInvalid(err))
		return
	}

	acc, err := rs.store.GetAccountByEmail(body.Email)
	if err != nil {
		log(r).Error(err)
		render.Render(w, r, ErrInternalServerError)
		return
	}
	if acc == nil || acc.Password == "" || !rs.checkPassword(body.Password, acc.Password) {
		render.Render(w, r, ErrInvalid(ErrInvalidLogin))
		return
	}

	token, err := rs.tokenAuth.CreateTokenForAccount(acc)
	if err != nil {
		log(r).Error(err)
		render.Render(w, r, ErrInternalServerError)
		return
	}
	render.Respond(w, r, &authResponse{
		AccountID: acc.ID,
		Token:     token,
	})
	return
}

type forgotPasswordRequest struct {
	Email string
}

func (body *forgotPasswordRequest) Bind(r *http.Request) error {
	return validation.ValidateStruct(body,
		validation.Field(&body.Email, validation.Required, is.Email),
	)
}

func (rs *LocalAuth) forgotPassword(w http.ResponseWriter, r *http.Request) {
	body := &forgotPasswordRequest{}
	if err := render.Bind(r, body); err != nil {
		render.Render(w, r, ErrInvalid(err))
		return
	}

	acc, err := rs.store.GetAccountByEmail(body.Email)
	if err != nil {
		log(r).Error(err)
		render.Render(w, r, ErrInternalServerError)
		return
	}
	if acc == nil {
		render.Render(w, r, ErrInvalid(ErrNoSuchEmail))
		return
	}

	code := generateRandomCode(10)
	err = rs.store.UpdateResetCode(acc.ID, code)
	if err != nil {
		log(r).Error(err)
		render.Render(w, r, ErrInternalServerError)
		return
	}

	to := email.NewEmail("User", acc.Email)
	err = rs.mailer.SendResetPasswordMail(to, code)
	if err != nil {
		log(r).Error(err)
		render.Render(w, r, ErrInternalServerError)
		return
	}
	return
}

type resetPasswordRequest struct {
	Email     string
	Password  string
	ResetCode string `json:"reset_code"`
}

func (body *resetPasswordRequest) Bind(r *http.Request) error {
	return validation.ValidateStruct(body,
		validation.Field(&body.Email, validation.Required, is.Email),
		validation.Field(&body.Password, validation.Required, passwordLengthValidation),
		validation.Field(&body.ResetCode, validation.Required),
	)
}

func (rs *LocalAuth) resetPassword(w http.ResponseWriter, r *http.Request) {
	body := &resetPasswordRequest{}
	if err := render.Bind(r, body); err != nil {
		render.Render(w, r, ErrInvalid(err))
		return
	}

	acc, err := rs.store.GetAccountByEmail(body.Email)
	if err != nil {
		log(r).Error(err)
		render.Render(w, r, ErrInternalServerError)
		return
	}
	if acc == nil {
		render.Render(w, r, ErrInvalid(ErrNoSuchEmail))
		return
	}

	if acc.ResetCode == "" || body.ResetCode != acc.ResetCode {
		render.Render(w, r, ErrInvalid(ErrInvalidResetCode))
		return
	}

	err = rs.store.UpdatePasswordReset(acc.ID, rs.hashPassword(body.Password))
	if err != nil {
		log(r).Error(err)
		render.Render(w, r, ErrInternalServerError)
		return
	}
	return
}

const codeChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

func generateRandomCode(n int) string {
	buf := make([]byte, n)
	if _, err := rand.Read(buf); err != nil {
		panic(err)
	}

	for k, v := range buf {
		buf[k] = codeChars[v%byte(len(codeChars))]
	}
	return string(buf)
}
