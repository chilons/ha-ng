package auth

import (
	"errors"
	"net/http"

	"github.com/go-chi/render"
)

// The list of error types presented to the end user as error message.
var (
	ErrTokenUnauthorized  = errors.New("token unauthorized")
	ErrTokenExpired       = errors.New("token expired")
	ErrInvalidAccessToken = errors.New("invalid access token")
	ErrInvalidLogin       = errors.New("invalid email or password")
	ErrNoSuchEmail        = errors.New("no such email")
	ErrInvalidResetCode   = errors.New("invalid reset code")
	ErrEmailInUse         = errors.New("email is already in use")
	ErrLoginToken         = errors.New("invalid or expired callback token")
)

// ErrResponse renderer type for handling all sorts of errors.
type ErrResponse struct {
	Err            error `json:"-"` // low-level runtime error
	HTTPStatusCode int   `json:"-"` // http response status code

	StatusText string `json:"status"`          // user-level status message
	AppCode    int64  `json:"code,omitempty"`  // application-specific error code
	ErrorText  string `json:"error,omitempty"` // application-level error message, for debugging
}

// Render sets the application-specific error code in AppCode.
func (e *ErrResponse) Render(w http.ResponseWriter, r *http.Request) error {
	render.Status(r, e.HTTPStatusCode)
	return nil
}

// ErrUnauthorized renders status 401 Unauthorized with custom error message.
func ErrUnauthorized(err error) render.Renderer {
	return &ErrResponse{
		Err:            err,
		HTTPStatusCode: http.StatusUnauthorized,
		StatusText:     http.StatusText(http.StatusUnauthorized),
		ErrorText:      err.Error(),
	}
}

// ErrInvalid renders status 400 Bad Request with custom error message.
func ErrInvalid(err error) render.Renderer {
	return &ErrResponse{
		Err:            err,
		HTTPStatusCode: http.StatusUnprocessableEntity,
		StatusText:     http.StatusText(http.StatusUnprocessableEntity),
		ErrorText:      err.Error(),
	}
}

// The list of default error types without specific error message.
var (
	ErrBadRequest          = &ErrResponse{HTTPStatusCode: http.StatusBadRequest, StatusText: http.StatusText(http.StatusBadRequest)}
	ErrInternalServerError = &ErrResponse{
		HTTPStatusCode: http.StatusInternalServerError,
		StatusText:     http.StatusText(http.StatusInternalServerError),
	}
)
