package app

import (
	"HA_NG/models"
	"context"
	"net/http"
	"strings"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"

	"HA_NG/database"
)

const profileCtxKey = "api_profile"

// ProfileResource provides profile resources and handlers.
type ProfileResource struct {
	store *database.ProfileStore
}

// NewAPI configures and returns ProfileResource.
func NewProfileResource(store *database.ProfileStore) *ProfileResource {
	return &ProfileResource{
		store: store,
	}
}

// Router provides profile routes.
func (rs *ProfileResource) Router() *chi.Mux {
	r := chi.NewRouter()
	r.Use(rs.profileCtx)
	r.Get("/", rs.get)
	r.Put("/", rs.update)
	return r
}

func (rs *ProfileResource) profileCtx(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		accID := accountID(r)
		p, err := rs.store.GetAccountProfile(accID)
		if err != nil {
			log(r).WithField("profile_ctx", accID).Error(err)
			log(r).Error(err)
			render.Render(w, r, ErrInternalServerError)
			return
		}
		ctx := context.WithValue(r.Context(), profileCtxKey, p)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func (rs *ProfileResource) getProfile(r *http.Request) *models.Profile {
	return r.Context().Value(profileCtxKey).(*models.Profile)
}

func (rs *ProfileResource) get(w http.ResponseWriter, r *http.Request) {
	render.Respond(w, r, rs.getProfile(r))
	return
}

type profileRequest struct {
	*models.Profile
}

func (body *profileRequest) Bind(r *http.Request) error {
	body.Email = strings.ToLower(strings.TrimSpace(body.Email))
	return body.Profile.Validate()
}

func (rs *ProfileResource) update(w http.ResponseWriter, r *http.Request) {
	p := rs.getProfile(r)
	email := p.Email
	body := &profileRequest{Profile: p}
	if err := render.Bind(r, body); err != nil {
		render.Render(w, r, ErrInvalid(err))
		return
	}

	err := rs.store.Update(p)
	if err != nil {
		log(r).Error(err)
		render.Render(w, r, ErrInternalServerError)
		return
	}

	if email != body.Email {
		ok, err := rs.store.UpdateAccountEmail(p.AccountID, body.Email)
		if err != nil {
			log(r).Error(err)
			render.Render(w, r, ErrInternalServerError)
			return
		}
		if !ok {
			render.Render(w, r, ErrInvalid(ErrEmailInUse))
			return
		}
	}

	render.Respond(w, r, p)
	return
}
