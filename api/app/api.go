// Package app ties together application resources and handlers.
package app

import (
	"HA_NG/database"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/jmoiron/sqlx"
	"github.com/sirupsen/logrus"

	"HA_NG/api/auth"
	"HA_NG/logging"
)

// LocalAuth provides application resources and handlers.
type API struct {
	profile *ProfileResource
}

// NewAPI configures and returns the application API.
func NewAPI(db *sqlx.DB) (*API, error) {
	profileStore := database.NewProfileStore(db)
	api := &API{
		profile: NewProfileResource(profileStore),
	}
	return api, nil
}

// Router provides application routes.
func (a *API) Router() *chi.Mux {
	r := chi.NewRouter()
	r.Mount("/profile", a.profile.Router())
	return r
}

func log(r *http.Request) logrus.FieldLogger {
	return logging.GetLogEntry(r)
}

func sessionData(r *http.Request) auth.SessionData {
	return auth.SessionDataFromContext(r.Context())
}

func accountID(r *http.Request) int64 {
	return sessionData(r).AccountID
}
