package models

import (
	"time"
)

// Account holds account information.
type Account struct {
	ID        int64     `json:"id"`
	Email     string    `json:"email"`
	Password  string    `json:"-"`
	CreatedAt time.Time `json:"created_at,omitempty" db:"created_at"`
	ResetCode string    `json:"-" db:"reset_code"`
}
