package models

import (
	"github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
)

// Profile holds profile information
type Profile struct {
	ID        int64  `json:"id"`
	AccountID int64  `json:"account_id" db:"account_id"`
	FullName  string `json:"full_name" db:"full_name"`
	Telephone string `json:"telephone"`
	Address   string `json:"address"`
	Email     string `json:"email"`
	CreatedAt string `json:"created_at" db:"created_at"`
	UpdatedAt string `json:"updated_at" db:"updated_at"`
}

// Validate the profile
func (p *Profile) Validate() error {
	return validation.ValidateStruct(p,
		validation.Field(&p.FullName, validation.Required),
		validation.Field(&p.Telephone, is.E164),
		validation.Field(&p.Email, validation.Required, is.Email),
	)
}
