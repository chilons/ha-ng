module HA_NG

require (
	github.com/BurntSushi/toml v0.3.1 // indirect
	github.com/GeertJohan/go.rice v0.0.0-20181229193832-0af3f3b09a0a
	github.com/asaskevich/govalidator v0.0.0-20180720115003-f9ffefc3facf // indirect
	github.com/daaku/go.zipexe v0.0.0-20150329023125-a5fe2436ffcb // indirect
	github.com/dghubble/sling v1.2.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-chi/chi v3.3.3+incompatible
	github.com/go-chi/cors v1.0.0
	github.com/go-chi/docgen v1.0.2
	github.com/go-chi/jwtauth v3.3.0+incompatible
	github.com/go-chi/render v1.0.1
	github.com/go-ozzo/ozzo-validation v3.5.0+incompatible
	github.com/go-sql-driver/mysql v1.4.0
	github.com/google/go-querystring v1.0.0 // indirect
	github.com/gorilla/securecookie v1.1.1
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/jmoiron/sqlx v1.2.0
	github.com/konsorten/go-windows-terminal-sequences v1.0.1 // indirect
	github.com/kr/pretty v0.1.0 // indirect
	github.com/mitchellh/go-homedir v1.0.0
	github.com/mitchellh/mapstructure v1.1.2 // indirect
	github.com/sendgrid/rest v2.4.1+incompatible // indirect
	github.com/sendgrid/sendgrid-go v3.4.1+incompatible
	github.com/sirupsen/logrus v1.1.1
	github.com/spf13/cobra v0.0.3
	github.com/spf13/pflag v1.0.3 // indirect
	github.com/spf13/viper v1.2.1
	golang.org/x/crypto v0.0.0-20181012144002-a92615f3c490
	golang.org/x/sys v0.0.0-20181011152604-fa43e7bc11ba // indirect
	google.golang.org/api v0.1.0
	google.golang.org/appengine v1.4.0 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
)
