// Package database implements mysql connection and queries.
package database

import (
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"github.com/spf13/viper"
)

// DBConn returns a postgres connection pool.
func DBConn() (db *sqlx.DB, err error) {
	db, err = sqlx.Connect("mysql", viper.GetString("database_url")+"?parseTime=true")
	if err != nil {
		return nil, err
	}

	if err := checkConn(db); err != nil {
		return nil, err
	}

	return db, nil
}

func checkConn(db *sqlx.DB) error {
	var n int
	if err := db.Get(&n, "SELECT 1"); err != nil {
		return err
	}
	return nil
}
