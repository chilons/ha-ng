package database

import (
	"database/sql"
	"github.com/jmoiron/sqlx"

	"HA_NG/models"
)

// AuthStore implements database operations for authentication.
type AuthStore struct {
	db *sqlx.DB
}

// NewAuthStore returns an AuthStore.
func NewAuthStore(db *sqlx.DB) *AuthStore {
	return &AuthStore{
		db: db,
	}
}

// GetAccountByEmail finds an account by Email, return nil if not found.
func (s *AuthStore) GetAccountByEmail(email string) (*models.Account, error) {
	var a models.Account
	err := s.db.Get(&a, "SELECT * FROM accounts WHERE email = ?", email)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	return &a, err
}

// CreateAccount creates an account with an empty profile.
func (s *AuthStore) CreateAccount(a *models.Account) (err error) {
	return s.CreateAccountWithProfile(a, &models.Profile{})
}

// CreateAccount creates an account along with a profile.
func (s *AuthStore) CreateAccountWithProfile(a *models.Account, p *models.Profile) (err error) {
	tx, err := s.db.Beginx()
	if err != nil {
		return
	}
	res, err := tx.NamedExec("INSERT INTO accounts (email, password) VALUES (:email, :password)", a)
	if err != nil {
		return
	}
	if a.ID, err = res.LastInsertId(); err != nil {
		return
	}
	p.AccountID = a.ID
	_, err = tx.NamedExec(`INSERT INTO profiles (account_id, full_name, address, telephone)
		VALUES (:account_id, :full_name, :address, :telephone)`, p)
	if err != nil {
		return
	}
	if err = tx.Commit(); err != nil {
		return
	}
	return
}

// UpdateResetCode updates the account's reset password code.
func (s *AuthStore) UpdateResetCode(accountID int64, resetCode string) (err error) {
	_, err = s.db.Exec("UPDATE accounts SET reset_code = ? WHERE id = ?", resetCode, accountID)
	return
}

// UpdatePasswordReset updates the account's password and removes the reset code.
func (s *AuthStore) UpdatePasswordReset(accountID int64, newPassword string) (err error) {
	_, err = s.db.Exec("UPDATE accounts SET password = ?, reset_code = '' WHERE id = ?", newPassword, accountID)
	return
}
