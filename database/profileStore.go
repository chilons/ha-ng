package database

import (
	"github.com/jmoiron/sqlx"

	"HA_NG/models"
)

// ProfileStore implements database operations for profile management by user.
type ProfileStore struct {
	db *sqlx.DB
}

// NewProfileStore returns an ProfileStore.
func NewProfileStore(db *sqlx.DB) *ProfileStore {
	return &ProfileStore{
		db: db,
	}
}

// GetAccountProfile finds a profile by account ID.
func (s *ProfileStore) GetAccountProfile(accountID int64) (*models.Profile, error) {
	var p models.Profile
	err := s.db.Get(&p,
		`SELECT profiles.*, accounts.email
			FROM profiles LEFT JOIN accounts ON accounts.id = account_id
			WHERE account_id = ?`,
		accountID)
	return &p, err
}

// Create a new profile.
func (s *ProfileStore) Create(p *models.Profile) (err error) {
	res, err := s.db.NamedExec("INSERT INTO accounts (email, password) VALUES (:email, :password)", p)
	if err != nil {
		return
	}
	p.ID, err = res.LastInsertId()
	return
}

// Update account email.
func (s *ProfileStore) UpdateAccountEmail(accountID int64, email string) (ok bool, err error) {
	var c int
	err = s.db.Get(&c, "SELECT COUNT(*) FROM accounts WHERE email = ?", email)
	if err != nil {
		return
	}
	if c > 0 {
		return false, nil
	}
	_, err = s.db.Exec(`UPDATE accounts SET email = ? WHERE id = ?`, email, accountID)
	return true, err
}

// Update profile.
func (s *ProfileStore) Update(p *models.Profile) (err error) {
	_, err = s.db.NamedExec(
		`UPDATE profiles
			SET full_name = :full_name,
			    telephone = :telephone,
			    address = :address,
			    updated_at = NOW()
			WHERE id = :id`, p)
	return
}
